const https = require('https');
const http = require('http');
const libxmljs = require("libxmljs");
const MongoClient = require('mongodb').MongoClient;
const url_module = require('url');



let one_page = (collection, absolute_url, callback) => {

    let dictionary = {};

    let target = new URL(absolute_url);
    let HTTP = null;
    if (target.protocol == "https:") {
        HTTP = https;
    } else if (target.protocol == "http:") {
        HTTP = http;
    } else {
        return;
    }

    const req = HTTP.request(absolute_url, (res) => {

        let data = [];
        res.on('data', (xml) => {
            data.push(xml);
        });

        res.on('end', () => {
            let xml = Buffer.concat(data);
            let target = xml.toString();

            let xmlDoc = "";
            if (target) {
                xmlDoc = libxmljs.parseHtmlString(target);
                let links = xmlDoc.find('//a');
                let divs = xmlDoc.get('//div');


                links.forEach(link => {
                    let attr = link.attr('href');
                    let relative_url = "";
                    if (attr) {
                        relative_url = attr.value();
                        dictionary[url_module.resolve(absolute_url, relative_url)] = "";
                    }
                });

                let text = "";
                if (divs) {
                    text = divs.text().replace(/\s+/g, "");
                }

                collection.insertOne({url: absolute_url, content: text}, (error, result) => {

                    let keys = Object.keys(dictionary);
                    keys.forEach(url => {
                        collection.find({url: url}).toArray((error, documents) => {
                            if (documents.length == 0) {
                                one_page(collection, url, (collection,url) => {


                                });
                            }
                        });
                    });


                });

            }
        });
    });

    req.on('error', (e) => {
        console.error(`problem with request: ${e.message}`);
    });

    req.end();
};


MongoClient.connect('mongodb://localhost:27017', (error, _client) => {

    let client = _client;
    let db = client.db('googol');
    let collection = db.collection("documents");

    one_page(collection, 'http://localhost:3000/index.html', (collection,url) => {

    });

});

