const conn = new WebSocket("wss://testnet-dex.binance.org/api/ws");
conn.onopen = function (evt) {
    conn.send(JSON.stringify({ method: "subscribe", topic: "allTickers", symbols: ["$all"] }));
};
conn.onmessage = function (evt) {
    let e = evt;
    console.info('received data', evt.data);
};
conn.onerror = function (evt) {
    console.error('an error occurred', null);
};
//# sourceMappingURL=test3.js.map