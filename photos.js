const http = require('http');
const https = require('https');
const libxmljs = require("libxmljs");
// const MongoClient = require('mongodb').MongoClient;
const url_module = require('url');
const path = require('path');
const semaphore = require('semaphore');

const _ = require("lodash");

// const MeCab = require('mecab-async');
// const mecab = new MeCab();

const sem = semaphore(4);	// 同時に４つのUrlにアクセス。増やしすぎるとリソースが足らない。
let count = 0;

let file_ext = (file_name) => {
	let result = "";
	if (file_name) {
		result = path.extname(file_name);
	}
	return result;
};

let file_name = (target_url) => {
	let result = "";
	let full_path = target_url.pathname;
	if (full_path) {
		let path_array = full_path.split('/');
		if (path_array.length > 0) {
			result = path_array[path_array.length - 1];
		}
	}
	return result;
};

let one_page = (collection, domain, unescaped_absolute_url) => {                                      						//　1ページ分のデータを処理する。
	try {
		const already = collection[unescaped_absolute_url];
		if (!already) {
			collection[unescaped_absolute_url] = true;
			const absolute_url = encodeURI(unescaped_absolute_url);
			const target_url = new URL(absolute_url);                                            				//　urlをパースして
			const host = target_url.host;
			if (host === domain) {
				const filename = file_name(target_url);
				const fileext = file_ext(filename);
				if (fileext !== ".jpg") {
					let network = null;
					if (target_url.protocol === "http:") {                                               				// プロトコルをチェック
						network = http;
					} else if (target_url.protocol === "https:") {                                               		// プロトコルをチェック
						network = https;
					}
					if (network) {																						// httpかhttpsならGo!

						// そしてまだデータベースに登録されていないページなら
						sem.take(() => {																	// 入れる数を制限する区域に入る。
							try {
								const req = network.request(absolute_url, (res) => {                    		// ホームページ配送してね。resっていうのは配達の人。
									const parts_box = [];                                                       // データの破片を入れる箱。
									res.on('data', (parts) => {                                   				// ネットからデータの破片が届いた...
										parts_box.push(parts);													// データの破片を箱に入れとく。
									});
									res.on('end', () => {                                       　				// 終わったよ。
										const html_data = Buffer.concat(parts_box);                               // 箱の中身を一つにまとめて...
										const html_string = html_data.toString();                                 // データを文字にするよ。
										if (html_string) {
											const html_doc = libxmljs.parseHtmlString(html_string);				// 文字を読んで、
											let links = null;
											let images = null;
											try {
												links = html_doc.find('//a');									// リンクタグを分類して箱に入れる。
												images = html_doc.find('//img');
											} catch {
												console.log("!!");
											}

											const dictionary = {};												// 辞書だよ。同じurlは上書きして一意にするため。
											if (links) {
												links.forEach(link => {
													const href = link.attr('href');									// Link箱の中身全部
													if (href) {
														const relative_url = href.value();								// リンク先を取り出して
														const resolved = url_module.resolve(absolute_url, relative_url);	// 絶対パスに直して
														dictionary[resolved] = "";   									// 辞書に入れとく。
													}
												});
											}

											if ((!already) && (res.statusCode === 200)) {

												const keys = Object.keys(dictionary);                                     // 索引全部
												keys.forEach(async (url) => {                                 // 辞書の索引全部のページの
													one_page(collection, domain, url);    										// 1ページ分のデータを処理する。

												});

												if (images) {
													images.forEach(element => {
														const href = element.attr('src');
														let href_text = "";
														if (href) {
															href_text = href.value();
														}

														const alt = element.attr('alt');
														let alt_text = "";
														if (alt) {
															alt_text = alt.value();
														}

														// console.log(href_text + " : " + alt_text);

													});
												}

												collection[unescaped_absolute_url] = images;
											 	console.log(unescaped_absolute_url);
											}

										}
										sem.leave();																//入る数を制限する区域おわり
									});
								}).on('error', (error) => {																//エラーなら
									console.error(`problem with request: ${error.message}`);							// エラーだよ！
									sem.leave();																	//入る数を制限する区域おわり
								});
								req.end();　　																		//1ページおわり
							} catch (e) {

							}
						});
					}
				}
			}
		}

	} catch (error) {
		console.log("exception : " + absolute_url + "error : " + error.message);
	}
};

//MongoClient.connect('mongodb://localhost:27017', {useNewUrlParser: true}, (error, _client) => {  // データベースを使うよ。
//	if (!error) {
		//	let client = _client;																	// データベースの代理人を呼び出す。
		//	let db = client.db('googol');													// 代理人に、"googol"という台帳を開いてもらって
		//	let collection = db.collection("articles");										// "articles"という台帳内の項目を見せてもらう。
		// one_page(collection, args);
		// const args = process.argv.pop();

		const collection = {};

		const absolute_url = encodeURI('https://motociclistagiapponese.com/');
		const target_url = new URL(absolute_url);                              //　urlをパースして
		const domain = target_url.host;
		one_page(collection, domain, absolute_url);								//最初のページ読んで？
//	}
//});
