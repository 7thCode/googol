// モジュールの取り込み
const MeCab = require('mecab-async');
const mecab = new MeCab();

// コマンドライン引数を取得
const args = process.argv.pop();

// 処理開始
mecab.parse(args, (error, result) => {
	if (!error) {
		console.log(result);
	}
});
