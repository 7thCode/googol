### パーソナル検索エンジン

## Googol
---
    プログラミングファンクラブの教材として開発してます。
---
### 必要なものは
    Node.js、MongoDB, mecabという感じ。
---
### 使い方.
```
    まず必要なものをインストールします。
```
---
#### node.js
```
バージョンは特になんでもいいです。
テストはv10.X.Xで行なっています。
```
---
#### MongoDB
```
バージョンは特になんでもいいです。
テストはv3.X.Xで行なっています。
```
---
#### Mecab
```
バージョンは特になんでもいいです。
```
---
### ページ収集.

```bash
$ git clone ... googol ...
$ cd googol
$ npm install
```
```
例えば、収集する起点のページを"https://news.google.com/?hl=ja&gl=JP&ceid=JP:ja"とすれば
```
```bash
$ node google.js https://news.google.com/?hl=ja&gl=JP&ceid=JP:ja
```
---
### 起動.

```bash
$ node bin/www
```
```
ブラウザからhttp://localhost:3000でアクセス。
```

```mermaid
    sequenceDiagram
      A ->> B  : 要求
      B -->> A : 返答
```

