/**
 * Copyright (c) 2019 7thCode.(http://seventh-code.com/)
 * This software is released under the MIT License.
 * opensource.org/licenses/mit-license.php
 */
"use strict";
var ImagesModel;
(function (ImagesModel) {
    const mongoose = require("mongoose");
    const crypto = require("crypto");
    const Schema = mongoose.Schema;
    const Src = new Schema({
        user_id: { type: String, default: "" },
        content: {
            id: { type: String, default: "" },
            src: { type: String, required: true },
            alt: { type: String, default: "" },
            url: { type: String, required: true },
            description: { type: String, default: "" },
            accessory: { type: Schema.Types.Mixed },
        },
    });
    const setId = (id) => {
        const idString = id.toString();
        const shasum = crypto.createHash("sha1");
        shasum.update(idString);
        return shasum.digest("hex");
    };
    Src.methods._create = function (user, content, cb) {
        //	this.user_id = user.user_id;
        this.content.id = setId(this._id);
        this.content = content;
        this.save(cb);
    };
    module.exports = mongoose.model("Src", Src);
})(ImagesModel || (ImagesModel = {}));
//# sourceMappingURL=images.js.map