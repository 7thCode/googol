var dns = require('dns');
var url = require('url');

const parse_url = (url_string, callback) => {
	const persed_url = url_module.parse(url_string, true);
	if (!persed_url.port) {
		if (!persed_url.auth) {
			const protocol = persed_url.protocol;
			switch (protocol) {
				case "http:":
				case "https:":
					dns.lookup(persed_url.host, (err, address, family) => {
						callback(null, protocol + "//" + address + persed_url.path);
					});
					break;
				default:
					callback({message: "protocol", code: -1}, null);
			}
		} else {
			callback({message: "auth", code: -1}, null);
		}
	} else {
		callback({message: "port", code: -1}, null);
	}
};

/*
  //protocol: 'http:',
  slashes: true,
  //auth: 'user:pass',
  //host: 'host.co.jp:3000',
  //port: '3000',
  //hostname: 'host.co.jp',
  hash: '#hash',
  search: '?query=string',
  query: 'query=string',
  pathname: '/p/a/t/h',
  path: '/p/a/t/h?query=string',
  href: 'http://user:pass@host.co.jp:3000/p/a/t/h?
* */
