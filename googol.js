const http = require('http');
const https = require('https');
const libxmljs = require("libxmljs");
const MongoClient = require('mongodb').MongoClient;
const url_module = require('url');
const semaphore = require('semaphore');

const _ = require("lodash");

const MeCab = require('mecab-async');
const mecab = new MeCab();

const sem = semaphore(4);	// 同時に４つのUrlにアクセス。増やしすぎるとリソースが足らない。
let count = 0;

let one_page = (collection, unescaped_absolute_url) => {                                      						//　1ページ分のデータを処理する。
	try {
		const absolute_url = encodeURI(unescaped_absolute_url);
		const target_url = new URL(absolute_url);                                            				//　urlをパースして
		let network = null;
		if (target_url.protocol === "http:") {                                               				// プロトコルをチェック
			network = http;
		} else if (target_url.protocol === "https:") {                                               		// プロトコルをチェック
			network = https;
		}
		if (network) {																						// httpかhttpsならGo!
			collection.findOne({url: absolute_url}, (error, already) => {
				if (!error) {
					//		if (!already) {																			// そしてまだデータベースに登録されていないページなら
					sem.take(() => {																	// 入れる数を制限する区域に入る。
						try {
							const req = network.request(absolute_url, (res) => {                    		// ホームページ配送してね。resっていうのは配達の人。
								const parts_box = [];                                                       // データの破片を入れる箱。
								res.on('data', (parts) => {                                   				// ネットからデータの破片が届いた...
									parts_box.push(parts);													// データの破片を箱に入れとく。
								});
								res.on('end', () => {                                       　				// 終わったよ。
									const html_data = Buffer.concat(parts_box);                               // 箱の中身を一つにまとめて...
									const html_string = html_data.toString();                                 // データを文字にするよ。
									if (html_string) {
										const html_doc = libxmljs.parseHtmlString(html_string);				// 文字を読んで、
										const links = html_doc.find('//a');									// リンクタグを分類して箱に入れる。
										const bodys = html_doc.find('//body');									// bodyタグを分類して箱に入れる。
										const title = html_doc.find('//title');									// titleタグを分類して箱に入れる。

										const dictionary = {};												// 辞書だよ。同じurlは上書きして一意にするため。
										links.forEach(link => {
											const href = link.attr('href');									// Link箱の中身全部
											if (href) {
												const relative_url = href.value();								// リンク先を取り出して
												const resolved = url_module.resolve(absolute_url, relative_url);	// 絶対パスに直して
												dictionary[resolved] = "";   									// 辞書に入れとく。
											}
										});

										const keys = Object.keys(dictionary);                                     // 索引全部
										keys.forEach(async (url) => {                                 // 辞書の索引全部のページの
											one_page(collection, url);    										// 1ページ分のデータを処理する。
										});

										if ((!already) && (res.statusCode === 200)) {
											let text = "";
											if (bodys) {                                                             // 一方、divタグがあったら
												bodys.forEach(element => {											// 全てのdivの
													const element_text = element.text().replace(/\s+/g, " ");	// スペースやタブや改行を取り除いて
													if (element_text) {										// 何かが残っていたら
														text += element_text;
													}
												});
											}

											if (text) {
												const limited_text = text.substr(0, 69833);	// 大きいとエラー。謎。
												mecab.parse(limited_text, function (error, result) {
													if (!error) {

														let words = [];
														result.forEach((mecab_element) => {
															if (mecab_element.length >= 9) {
																const word_elements = [mecab_element[0], mecab_element[7] || "*", mecab_element[8] || "*", mecab_element[9] || "*"];　// mecabの出力から単語だけ取り出す。
																const filterd_word = _.filter(word_elements, function (word) { // 単語から、不要な*を省く
																	return (word !== "*")
																});
																words = _.union(words, filterd_word);
															}
														});

														const unique_words = _.uniq(words);　// 一意に。

														let page_title = "";
														if (title.length > 0) {
															page_title = title[0].text();
														}
														if (unique_words.length > 0) {
															collection.findOneAndReplace({url: absolute_url}, {
																url: absolute_url,
																title: page_title,
																description: "",
																content: unique_words
															}, {upsert: true}, (error, result) => {								// DBへ。
																if (!error) {
																	count++;
																	console.log(count);
																} else {
																	console.log(error.message);
																}
															});
														}
													}
												});
											}
										}

									}
									sem.leave();																//入る数を制限する区域おわり
								});
							}).on('error', (error) => {																//エラーなら
								console.error(`problem with request: ${error.message}`);							// エラーだよ！
								sem.leave();																	//入る数を制限する区域おわり
							});
							req.end();　　																		//1ページおわり
						} catch (e) {

						}
					});
					//		}
				}
			})
		}
	} catch (error) {
		console.log("exception : " + absolute_url + "error : " + error.message);
	}
};

MongoClient.connect('mongodb://localhost:27017', {useNewUrlParser: true}, (error, _client) => {  // データベースを使うよ。
	if (!error) {
		let client = _client;																	// データベースの代理人を呼び出す。
		let db = client.db('googol');													// 代理人に、"googol"という台帳を開いてもらって
		let collection = db.collection("articles");										// "articles"という台帳内の項目を見せてもらう。

		const args = process.argv.pop();

		one_page(collection, args);								//最初のページ読んで？
		// one_page(collection, 'https://nodejs.org/ja/');								//最初のページ読んで？
	}
});
