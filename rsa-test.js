const Rsa = require('./rsa');

const rsa =  new Rsa(512);

console.log("private key\n");
const private_key = rsa.privateKey();
console.log(private_key);

console.log("public key\n");
const public_key = rsa.publicKey();
console.log(public_key);


// const enc = rsa.Encrypt(public_key, "ほげええ");
// console.log(enc);
//
// const dec = rsa.Decrypt(private_key, enc);
// console.log(dec);
//
