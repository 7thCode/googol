const NodeRSA = require('node-rsa');

class Rsa {

    private key: any;

    constructor(bits: number) {
        this.key = new NodeRSA({b: bits});
    }

    public privateKey(): string {
        return this.key.exportKey('pkcs1-private-pem');
    }

    public publicKey(): string {
        return this.key.exportKey('pkcs1-public-pem');
    }

    public Encrypt(key: string, input: string): string {
        const rsa = new NodeRSA(key, 'pkcs1-public-pem', {encryptionScheme: 'pkcs1_oaep'});
        return rsa.encrypt(input, 'base64');
    }

    public Decrypt(key: string, input: string): string {
        const rsa = new NodeRSA(key, 'pkcs1-private-pem', {encryptionScheme: 'pkcs1_oaep'});
        return rsa.decrypt(input, 'utf8');
    }

}

module.exports = Rsa;
