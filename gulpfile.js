var gulp = require('gulp');
var typescript = require('gulp-typescript');
var sourcemaps = require('gulp-sourcemaps');

var rimraf = require('rimraf');

gulp.task('clean', function(cb) {
	rimraf('product', cb);
});

gulp.task('compile', function(){

	return gulp.src([
		'app.ts',
		'models/**/*.ts',
		'server/**/*.ts',
		'types/**/*.ts'
	], { base: './' })
		.pipe(sourcemaps.init())
		.pipe(typescript({ target: "ES5", removeComments: true }))
		.js
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest('./'));

});

