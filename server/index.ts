var express = require('express');
const MongoClient = require('mongodb').MongoClient;

var router = express.Router();

var Crawler = require('./crawler').Crawler;

const mongoose = require("mongoose");

const options = {
	keepAlive: 1,
	connectTimeoutMS: 1000000,
	useNewUrlParser: true
};

mongoose.connect("mongodb://localhost/googol", options);

const Images = require("../models/images/images");

/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('index', {title: 'Express'});
});

router.get('/query/:q', function(req, res, next) {
	const query = decodeURIComponent(req.params.q);
	let query_object = {};
	try {
		query_object = JSON.parse(query);
	} catch (e) {
		console.log(e.message);
	}

	Images.find(query_object, {}, {}, (error, results) => {
		if (!error) {
			res.json(results);
		} else {
			res.send("error");
		}
	});
});

router.get('/get/:id', function(req, res, next) {
	const id = req.params.id;
});

router.post('/write', function(req, res, next) {
	const body = req.body;
});

router.put('/update/:id', function(req, res, next) {
	const id = req.params.id;
	const body = req.body;
});

router.delete('/delete/:id', function(req, res, next) {
	const id = req.params.id;

});

router.get('/test', function(req, response, next) {
	MongoClient.connect('mongodb://localhost:27017', {useNewUrlParser: true}, (error, _client) => {
		if (!error) {
			let client = _client;
			let db = client.db('googol');
			let collection = db.collection("src");

			const args = process.argv.pop();

			response.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'});
			response.write("<div>ok</div>");
			response.end();

			const crawler = new Crawler({ignore_ext: [".jpg", ".jpeg", '.png', '.mp4'], multiplicity:4});
			crawler.Crawl('https://motociclistagiapponese.com/', (error, result: { event: string, url: string, images: any[], queue: number }) => {
				if (!error) {
					switch (result.event) {
						case "resolve":
							console.log("queue: " + result.queue + "  url: " + result.url);
							if (result.images) {
								result.images.forEach(element => {

									const src = element.attr('src');
									let src_text = "";
									if (src) {
										src_text = src.value();
									}

									const alt = element.attr('alt');
									let alt_text = "";
									if (alt) {
										alt_text = alt.value();
									}

									collection.findOneAndReplace({"content.src": src_text}, {
										user_id: "",
										content: {
											id: "",
											src: src_text,
											alt: alt_text,
											url: result.url,
											description: "",
										}
									}, {upsert: true}, (error, result) => {
										if (!error) {

										} else {
											console.log(error.message);
										}
									});
								});
							}
							break;
						case "end":
							console.log("end");
							break;
					}
				} else {
					console.log(error.message);
				}
			});
		}
	});
});

module.exports = router;
