// sort without compare at item to item.
// For small numbers sort. O(n*m).

const input: number[] = [];
const output: number[] = [];

for (var sample_size = 0; sample_size < 100; sample_size++) {
	input.push(Math.floor(Math.random() * 120));
}

console.log(JSON.stringify(input));

const size: number = input.length;
let remain: number = size;
for (let counter: number = 0; remain > 0; counter++) { // counter = 現在の値
	for (let index: number = 0; index < size; index++) { //　index = 入力の番目
		if (input[index]-- === 0) {
			output.push(counter);
			remain--;
		}
	}
}

console.log(JSON.stringify(output));






