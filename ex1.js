const dialogflow = require('dialogflow');
const uuid = require('uuid');

// export GOOGLE_APPLICATION_CREDENTIALS="/Users/oda/google/test-4a600b731123.json"

async function exec(projectId = 'test-cee37') {

    const sessionId = uuid.v4();
    const sessionClient = new dialogflow.SessionsClient();
    const sessionPath = sessionClient.sessionPath(projectId, sessionId);
    const request = {
        session: sessionPath,
        queryInput: {
            text: {
                text: process.argv[2],
                languageCode: 'ja-JP',
            },
        },
    };

    const responses = await sessionClient.detectIntent(request);
    const result = responses[0].queryResult;
    if (result.intent) {
        console.log(`主題: ${result.intent.displayName}`);
    } else {
        console.log("質問意図不明");
    }
    console.log(`質問: ${result.queryText}`);
    console.log(`回答: ${result.fulfillmentText}`);
    console.log(JSON.stringify(result));

}

exec().then(() => {
    console.log("終了");
});